cookiecutter-django
=======================

Features
---------

* For Django 1.10.1
* Twitter Bootstrap_ 3
* Settings management via django-configurations_
* Registration via django-allauth_
* Gulp build for compass and jshint
* Basic e-mail configurations for send emails via SendGrid_
* Exception monitoring enabled with sentry.io

.. _Bootstrap: https://github.com/twbs/bootstrap
.. _django-configurations: https://github.com/jezdez/django-configurations
.. _django-allauth: https://github.com/pennersr/django-allauth
.. _django-avatar: https://github.com/jezdez/django-avatar/
.. _SendGrid: https://sendgrid.com/
.. _Sentry: https://sendgrid.com/

Usage
------

First, get cookiecutter:

    $ pip install cookiecutter

Now run it against this repo::

    $ cookiecutter https://github.com/naglalakk/cookiecutter-django.git

You'll be prompted for some questions, answer them, then it will create a Django project for you.

Setup
-----

	# cd into the folder you just created with cookiecutter
	cd project_folder

	# Install local requirements
	pip install -r requirements/local.txt

Make sure that you have DATABASE_URL environmental variable set.
Then run:

	source /project_folder/repo_name/ENV
	./project_folder/repo_name/bootstrap.sh

This will run all migrations and install static assets.
Note that you don't have to run source on that ENV file.
As long as you provide the DJANGO_SETTINGS_MODULE and
DJANGO_CONFIGURATION environmental variables you can run
the bootstrap script.

Start the server

	python manage.py runserver

Configurations
--------------
This project uses django-configurations to split up environments. The environments are:

	Local - Environment with configurations for local development
	Production - Environment with configurations for production environment
	Testing - Environment with configurations for test reports, coverage

Environmental variables
-----------------------

Every project comes with an .env file. This file should always be ignored by git
because it keeps passwords and keys that you don't want other people to know about.
The file comes with a default SECRET_KEY.
Optionally you can add pg_username and pg_password if you are using postgres.

Other environmental variables are

| Environment variable | default | required | info | Environment |
| ---------------------|:-------:| --------:| ----:| -----------:|
| AWS_ACCESS_KEY_ID | None | false | Amazon Access Key Id | Production
| AWS_SECRET_ACCESS_KEY | None | false | Amazon Secret Access Key | Production
| AWS_STORAGE_BUCKET_NAME | None | false | Name of S3 bucket on AWS | Production
| DJANGO_CONFIGURATION | Local | false | Django configuration class | ( ALL )
| DJANGO_SETTINGS_MODULE | config.local | false | Django settings module | ( config.local |  config.production | config.testing )
| EMAIL_BACKEND | django.core.mail.backends.smtp.EmailBackend | false | Email backend for sending mail | Production
| SECRET_KEY | Provided by cookieciutter | true | SECRET_KEY FOR PROJECT | ( ALL )
| SENDGRID_USER | None | false | Sendgrid user name | Production
| SENDGRID_PASSWORD | None | false | Sendgrid password | Production
