import factory
import random
from faker import Factory
fake = Factory.create()


from .models import User

class UserFactory(factory.DjangoModelFactory):
    first_name = factory.Sequence(lambda n: fake.first_name())
    last_name = factory.Sequence(lambda n: fake.last_name())
    email = factory.LazyAttribute(
        lambda o: "%s@dv.is" % o.first_name.lower()
    )
    username = factory.LazyAttribute(
        lambda o: o.first_name.lower()
    )
    is_active = True

    class Meta:
        model = 'users.User'
        django_get_or_create = ('username',)

class AdminUserFactory(UserFactory):
    """
    AdminUserFactory is for users who should have
    access to the custom admin
    """
    is_staff = True
<<<<<<< HEAD

=======
>>>>>>> reverting
