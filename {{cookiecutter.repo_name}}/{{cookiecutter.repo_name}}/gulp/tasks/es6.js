var gulp = require('gulp');
var config = require('../config');
var babel = require('gulp-babel');

gulp.task('es6', function () {
    return gulp.src(config.js.files)
        .pipe(babel())
        .pipe(gulp.dest(config.js.dest));
});

gulp.task('babel', ['eslint', 'es6']);
