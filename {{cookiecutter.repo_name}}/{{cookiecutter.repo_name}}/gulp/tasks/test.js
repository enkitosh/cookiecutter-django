/**
 * Run tests.
 *
 * gulp test            -- runs unit/functional tests
 * gulp test:unit       -- runs unit tests
 * gulp test:functional -- runs functional tests
 */

var gulp = require('gulp'),
    config = require('../config').test;

gulp.task('test-unit', shell.task([
    config.runner.unit
]));

gulp.task('test-functional', shell.task([
    config.runner.functional
]));

gulp.task('test', ['test-unit', 'test-functional']);
gulp.task('test:unit', ['test-unit']);
gulp.task('test:functional', ['test-functional']);
