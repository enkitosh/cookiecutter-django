var gulp = require('gulp');
var config = require('../config');
var react = require('gulp-react');
 
gulp.task('react', function () {
    return gulp.src(config.jsx.files)
        .pipe(react())
        .pipe(gulp.dest(config.jsx.dest));
});
