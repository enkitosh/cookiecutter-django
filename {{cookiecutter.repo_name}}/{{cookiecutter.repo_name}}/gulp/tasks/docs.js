/**
 * Generates documentation
 *      js:     jsDoc
 *      scss:   sassdoc
 *
 */
var gulp = require('gulp'),
    config = require('../config'),
    sassdoc = require('sassdoc'),
    validateJsDoc = require('gulp-validate-jsdoc');
    folderToc = require('folder-toc'),
    shell = require('gulp-shell'),
    runSequence = require('run-sequence');

gulp.task('validateJsDocs', function() {
    return gulp.src(config.js.files)
           .pipe(validateJsDoc());
});

gulp.task('jsdoc', shell.task([
    'jsdoc ' + config.js.src + ' -r -d ' + config.docs.js.src
]));

gulp.task('sassdoc', function() {
    return gulp.src(config.scss.files)
           .pipe(sassdoc({dest: config.docs.scss.src, verbose:true}))
});

gulp.task('docs', ['jsdoc', 'sassdoc']);
    
