var gulp         = require('gulp');
var sass         = require('gulp-ruby-sass');
var config       = require('../config').scss;

gulp.task('sass', function () {
    return sass(config.files)
        .on('error', sass.logError)
        .pipe(gulp.dest(config.dest));
});
