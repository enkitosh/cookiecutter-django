var gulp = require('gulp');
var config = require('../config');
var jshint = require('gulp-jshint');

gulp.task('jshint', function() {
    return gulp.src(config.js.files)
           .pipe(jshint())
           .pipe(jshint.reporter('jshint-stylish'));
});
