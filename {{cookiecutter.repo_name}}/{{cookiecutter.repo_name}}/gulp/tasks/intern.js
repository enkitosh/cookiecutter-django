/**
 * Intern test runner
 * By default, intern-client is run 
 * along with the config file declared in 
 * config.test.intern.configFile if one exists.
 * This command takes two optional arguments:
 *      --executor = client | runner
 *      --config   = /path/to/intern/config
 * */
var gulp = require('gulp'),
    argv = require('yargs').argv,
    shell = require('gulp-shell');

gulp.task('intern', shell.task([
    argv.executor + ' ' + argv.config
]));
