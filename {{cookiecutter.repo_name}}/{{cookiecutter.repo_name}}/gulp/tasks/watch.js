var gulp     = require('gulp');
var config   = require('../config');

gulp.task('watch', function() {
  gulp.watch(config.js.files,   ['jshint']);
  gulp.watch(config.scss.files,   ['sass']);
});
