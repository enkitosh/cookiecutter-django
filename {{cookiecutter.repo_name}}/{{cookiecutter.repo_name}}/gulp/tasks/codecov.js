var gulp = require('gulp');
var codecov = require('gulp-codecov.io');
 
gulp.task('codecov', function() {
    return gulp.src('./coverage/lcov.info')
      .pipe(codecov());
});
