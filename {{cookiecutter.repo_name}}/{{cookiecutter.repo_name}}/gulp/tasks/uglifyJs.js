var gulp    = require('gulp');
var config  = require('../config');
var size    = require('gulp-filesize');
var uglify = require('gulp-uglify');

gulp.task('uglifyJs', function() {
  return gulp.src(config.js.files)
    .pipe(uglify())
    .pipe(gulp.dest(config.dest))
    .pipe(size());
});
