var base = 'static';
var dest = base + "dist/";

module.exports = {
  css: {
    src: base + '/css',
    files: ['*.css']
  },
  docs: {
    src: base + '/docs',
    js: {
        src: base + '/docs/js'
    },
    scss: {
        src: base + '/docs/scss'
    }
  },
  images: {
    src: base + "/images",
    dest: dest + "/images"
  },
  js: {
    src: base + '/js',
    dest: base + '/js',
    files: [base + '/js/modules/*.js', base + '/js/modules/**/*.js']
  },
  jsx: {
    src: base + '/jsx',
    dest: base + '/js/jsx',
    files: [base + '/jsx/*.jsx', base + '/jsx/**/*.jsx']
  },
  scss: {
    src: base + '/scss',
    dest: base + '/css',
    files: [base + '/scss/*.{sass,scss}']
  },
  test: {
    runner: {
        unit: 'intern-client config=' + base + '/js/test/intern',
        functional: ''
    }
  }
};
