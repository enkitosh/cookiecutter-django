# -*- coding: utf-8 -*-
'''
Local Configurations
'''
from .common import Common


class Local(Common):

    # DEBUG
    DEBUG = True
    TEMPLATE_DEBUG = DEBUG
    # END DEBUG

    # INSTALLED_APPS
    INSTALLED_APPS = Common.INSTALLED_APPS
    # END INSTALLED_APPS


    # django-debug-toolbar
    INTERNAL_IPS = ('127.0.0.1',)

    # Mail settings
    ACCOUNT_EMAIL_VERIFICATION = "none"
    EMAIL_HOST = "localhost"
    EMAIL_PORT = 1025
    EMAIL_BACKEND = Common.env('DJANGO_EMAIL_BACKEND', default="django.core.mail.backends.console.EmailBackend")

