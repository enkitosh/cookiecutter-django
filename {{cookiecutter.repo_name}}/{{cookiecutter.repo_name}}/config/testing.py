class DisableMigrations(object):

    def __contains__(self, item):
        return True

    def __getitem__(self, item):
        return "notmigrations"

from .local import Local

class Testing(Local):
    """
    Disables migrations when running tests
    (can save a lot of time)
    """
    MIGRATION_MODULES = DisableMigrations()

    INSTALLED_APPS = Local.INSTALLED_APPS
    INSTALLED_APPS += ('django_nose',)

    # Use django-nose as test runner
    TEST_RUNNER='django_nose.NoseTestSuiteRunner'
    NOSE_ARGS = [
        '--with-coverage',
        '--cover-xml'
    ]
